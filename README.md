![Build Status](https://gitlab.com/pages/plain-html/badges/master/build.svg)

# URL
https://jsva.gitlab.io/assignment-4/

# Assignment 4
**Experis Academy Accelerate Course**
*Jonas Svåsand*

A website that pulls laptopinformation from an API.
It allows you to view the information of the different laptops.
You can purchase a laptop if you have sufficient funds in your bank.
In order to get money you can work, and deposit your pay to the bank.
If you need more money you can get a loan from the bank of up to 2x your current bank balance.
Only one loan is allowed at a time, and if you want to deposit your pay, 10% will be deducted towards paying off your loan.
You can also pay off your loan with all your pay, with the pay loan button. 
Lent money is deposited in your bank balance and can be used towards a laptop purchase.

The website is also hosted aon gitlab pages and can be visited there.
Permanent link to website on gitlab pages: https://jsva.gitlab.io/assignment-4/


### Usage
visit the URL https://jsva.gitlab.io/assignment-4/ in order to use the page

## GitLab CI

This project's static Pages are built by [GitLab CI][ci], following the steps
defined in [`.gitlab-ci.yml`](.gitlab-ci.yml):


