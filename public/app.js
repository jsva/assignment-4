//RETRIEVE AND STORE DOM ELEMENTS

const bankBalanceValueElement = document.getElementById("bank-balance-value");
const loanAmountElement = document.getElementById("loan-amount");
const loanAmountValueElement = document.getElementById("loan-amount-value");
const getLoanButtonElement = document.getElementById("get-loan-button");

const workPayValueElement = document.getElementById("work-pay-value");
const payLoanButtonElement = document.getElementById("pay-loan-button");
const depositButtonElement = document.getElementById("deposit-button");
const workButtonElement = document.getElementById("work-button");

const selectLaptopElement = document.getElementById("select-laptop");
const laptopFeaturesElement = document.getElementById("laptop-features");

const laptopImageElement = document.getElementById("laptop-image");
const laptopNameElement = document.getElementById("laptop-name");
const laptopDescriptionElement = document.getElementById("laptop-description");
const laptopPriceValueElement = document.getElementById("laptop-price-value");
const buyLaptopButtonElement = document.getElementById("buy-laptop-button");

//BANK VARIABLES
let balance = 0;
let loan = 0;

//BANK  BUTTON FUNCTIONS

function getLoan() {
  // Checks if a user is eligible for a loan, if they are it prompts the user for more information and validates
  if (loan) {
    //Check if loan is truthy, if it is don't allow a new loan
    alert("You already have a loan! Pay it back before you get another.");
  } else {
    let amount = parseInt(prompt("How much do you wish to borrow?", "1"), 10); // prompt user for how much they wish to lend and parse the response to int
    console.log(amount);
    if (amount > balance * 2) {
      // check on the loan amount is within the allowed parameters, if not tell them that they have to try again
      alert(
        "You cannot loan more than twice your current bank balance! Please try again with a appropiate loan amount"
      );
    } else if (isNaN(amount)) { // If the value was not a number, then give error message.
      alert('What you entered was not a valid number, please try again')
    } 
    else {
      // if everything is okay, update the users bank balance and loan amount
      loan = amount;
      balance += amount;
      updateBalance();
      updateLoan();
    }
  }
}
//WORK VARIABLES
let pay = 0;
const wage = 100;

//work button funcs

function payLoan() {
  // Pays off the loan with available balance from the users pay.
  if (loan >= pay) {
    //if the loan amount is greater than or equal to the current pay, use the whole pay
    loan -= pay;
    pay = 0;
  } else {
    // if the pay is larger than the loan, pay off the whole loan, leave the rest of the pay in the pay balance
    pay -= loan;
    loan = 0;
  }
  updatePay();
  updateLoan();
}

function deposit() {
  // Deposits all the pay into the deposit, if there is an active loan 10% will be deducted towards loan repayment
  let toDeposit = 0;
  if (loan) {
    // should be 'truthy' as long as there is a loan, but I can't let loan become negative.
    if (loan > pay * 0.1) {
      toDeposit = pay * 0.9;
      loan -= pay * 0.1;
    } else {
      toDeposit = pay - loan;
      loan = 0;
    }
  } else {
    toDeposit = pay;
  }
  balance += toDeposit;
  pay = 0;
  updatePay();
  updateBalance();
  updateLoan();
}

function work() {
  // function that gives the users one wage per time this func is called
  pay += wage;
  updatePay();
}

// functions that work with the Laptops
function buyLaptop() {
  let laptopPrice = laptopsById[currentLaptopId].price;
  if (balance < laptopPrice) {
    alert("You have insufficient funds to purchase this laptop");
  } else {
    balance -= laptopPrice;
    alert(`Congratulations you have purchased a ${laptopsById[currentLaptopId].title} laptop  for ${laptopPrice} NOK.\n
      Your remaining bank balance is ${balance} NOK`);
    updateBalance();
  }
}

//DOM MANIPULATION FUNCTIONS
// All these functions update the dom with the current value of different variables.
// Is called in the relevant functions so the newest value is always displayed.
function updatePay() {
  workPayValueElement.innerText = pay;
}

function updateBalance() {
  bankBalanceValueElement.innerText = balance;
}

function updateLoan() {
  loanAmountValueElement.innerText = loan;
  // has to also check if there is need to hide or show the loan elements
  if (loan <= 0) {
    loanAmountElement.style.display = "none";
    payLoanButtonElement.style.display = "none";
  } else {
    loanAmountElement.style.display = "block";
    payLoanButtonElement.style.display = "block";
  }
}

function updateFeatures() {
  // Updates the features of the laptop under the select laptop menu
  laptopFeaturesElement.innerHTML = "";
  let features = laptopsById[currentLaptopId].specs;
  features.forEach((feature) => {
    let newFeatureElement = document.createElement("li");
    newFeatureElement.innerText = feature;
    laptopFeaturesElement.appendChild(newFeatureElement);
  });
}

function updateImage() {
  //updates the laptop image and also adds correct alt text
  laptopImageElement.src =
    "https://noroff-komputer-store-api.herokuapp.com/" +
    laptopsById[currentLaptopId].image;
  laptopImageElement.alt = laptopsById[currentLaptopId].title;
}

function updateAllLaptopInfo() {
  // A function that updates all laptop information based on current laptop selection
  updateFeatures();
  updateImage();
  laptopNameElement.innerHTML = laptopsById[currentLaptopId].title;
  laptopDescriptionElement.innerHTML = laptopsById[currentLaptopId].description;
  laptopPriceValueElement.innerHTML = laptopsById[currentLaptopId].price;
}

//Event Listeners
workButtonElement.addEventListener("click", work);
depositButtonElement.addEventListener("click", deposit);
getLoanButtonElement.addEventListener("click", getLoan);
payLoanButtonElement.addEventListener("click", payLoan);
selectLaptopElement.addEventListener("change", handleLaptopSelectionChange);
buyLaptopButtonElement.addEventListener("click", buyLaptop);

//LAPTOP VARIABLES
const apiURL = "https://noroff-komputer-store-api.herokuapp.com/computers";

let laptopsById = {}; // a js object that contains all the objects with laptop information retrieved from API, the keys are the id of the laptop
let currentLaptopId = 1; //laptop with id = 1 should be default. this is an assumption, but should be fine for this assignment
let currentLaptopPrice = 0; //start with a dummy value but will be updated on start as well when you  select different laptops

//FETCH LAPTOP DATA FROM API

fetch(apiURL)
  .then((response) => response.json())
  .then((data) => addLaptops(data))
  .catch((error) => console.log(error));

const addLaptops = (laptops) => {
  laptops.forEach((laptop) => addLaptop(laptop));
  updateAllLaptopInfo(); //Call it once so that the information is loaded when you initially open the site. the current is set to 1 initially
};

const addLaptop = (laptop) => {
  //add options to dropdown menu
  let id = laptop.id;
  const laptopOptionElement = document.createElement("option"); //Creates a new option for the dropdown menu for the laptop its currently saving the info for
  laptopOptionElement.value = id;
  laptopOptionElement.appendChild(document.createTextNode(laptop.title));
  selectLaptopElement.appendChild(laptopOptionElement);
  laptopsById[id] = laptop;
  if (id == 5) {
    // Corrects the broken image link of laptop with id 5
    laptopsById[5].image = "assets/images/5.png";
  }
};

function handleLaptopSelectionChange(e) {
  //function that is activated when dropdown menu is changed.
  const selectedLaptopId = e.target.value; //retrieves id
  currentLaptopId = selectedLaptopId; // sets id as global variable
  updateAllLaptopInfo(); //update all laptop info with the new id
}
